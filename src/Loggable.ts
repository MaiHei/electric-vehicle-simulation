import { Logger, createLogger, transports, format } from 'winston';
import { red, yellow, blue, magenta } from "colors";
const { combine, timestamp, printf, metadata } = format;

export class Loggable {

  protected logger: Logger;

  private static basicInstance: Logger;
  private static initialized = false;

  constructor(private readonly loggerIndicator: string, private readonly isServer = false) {
    if (!Loggable.initialized) Loggable.init();
    if (this.isServer) {
      this.logger = createLogger({
        transports: [
          new transports.File({
            level: 'info',
            filename: './logs/server.log',

            format: combine(
              timestamp(),
              metadata({ fillWith: ["loggerIndicator"] }),
              Loggable.fileFormat
            ),
            handleExceptions: true,
            maxsize: 5242880, //5MB
            maxFiles: 5
          }),
          new transports.Console({
            level: 'debug',
            format: combine(
              timestamp(),
              metadata({ fillWith: ["loggerIndicator"] }),
              Loggable.consoleFormat
            ),
            handleExceptions: true,
          })
        ],
        defaultMeta: { loggerIndicator },
        exitOnError: false
      });
    } else {
      this.logger = Loggable.basicInstance.child({ loggerIndicator: this.loggerIndicator });
    }
  }

  private static fileFormat = printf(({ level, message, timestamp, metadata }) => {
    const ts = timestamp.replace("T", " ").replace("Z", "");
    return `${ts} - [${level}] ${metadata.loggerIndicator}: ${message}`;
  })

  private static consoleFormat = printf(({ level, message, timestamp, metadata }) => {
    const ts = timestamp.replace("T", " ").replace("Z", "");
    let lv;
    switch (level) {
      case "info":
        lv = blue(level);
        break;
      case "warning":
        lv = yellow(level);
        break;
      case "error":
        lv = red(level);
        break;
      default:
        lv = magenta(level);
    }

    return `${ts} - [${lv}] ${metadata.loggerIndicator}: ${message}`;
  })

  private static init() {
    this.basicInstance = createLogger({
      transports: [
        new transports.File({
          level: 'info',
          filename: './logs/combined.log',
          format: combine(
            timestamp(),
            metadata({ fillWith: ["loggerIndicator"] }),
            this.fileFormat
          ),
          handleExceptions: true,
          maxsize: 5242880, //5MB
          maxFiles: 5
        }),
        new transports.File({
          level: 'error',
          filename: './logs/error.log',
          format: combine(
            timestamp(),
            metadata({ fillWith: ["loggerIndicator"] }),
            this.fileFormat
          ),
          handleExceptions: true,
          maxsize: 5242880, //5MB
          maxFiles: 5
        }),
        new transports.Console({
          level: 'debug',
          format: combine(
            timestamp(),
            metadata({ fillWith: ["loggerIndicator"] }),
            this.consoleFormat
          ),
          handleExceptions: true,
        })
      ],
      exitOnError: false
    });
    this.initialized = true;
  }
}
