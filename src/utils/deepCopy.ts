export function deepCopy<T>(origin: T): T {
  const copy = JSON.parse(JSON.stringify(origin));
  // TODO: copy function-pointer from origin to copy
  return copy;
}

export default deepCopy;
