export function deepCompare(a: any, b: any): boolean {
  // TODO: sort keys before comparison.
  // JSONs are not sorted by keys, but by the time the key is created.
  return JSON.stringify(a) === JSON.stringify(b);
}

export default deepCompare;
