import { Router } from "express";

const SimulationRouter = Router();

SimulationRouter.get("/", (_req, res) => {
  res.send("simulation");
})

export default SimulationRouter;
