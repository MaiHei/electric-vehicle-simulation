import { Router } from "express";

const ResultRouter = Router();

ResultRouter.get("/", (_req, res) => {
  res.send("result");
})

export default ResultRouter;
