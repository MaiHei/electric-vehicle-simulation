import { Router } from "express";

const ModuleRouter = Router();

ModuleRouter.get("/", (_req, res) => {
  res.send("module");
})

export default ModuleRouter;
