import { Router } from "express";

const MessageRouter = Router();

MessageRouter.get("/", (_req, res) => {
  res.send("messages");
})

export default MessageRouter;
