import * as express from 'express'
import * as logger from 'morgan'
import * as bodyParser from 'body-parser'
import * as cors from 'cors'
import { Loggable } from '../Loggable'
import MessageRouter from './routes/MessageRoute';
import ModuleRouter from './routes/ModuleRoute';
import SimulationRouter from './routes/SimulationRoute';
import ResultRouter from './routes/ResultRoute';
import { Application, Router } from 'express';
import { join } from 'path';


/**
 * A Webserver
 *
 * @export
 * @class Server
 * @extends {Loggable}
 */
export class Server extends Loggable {

  /**
   * The servers instance.
   *
   * @private
   * @type {Application}
   * @memberof Server
   */
  private express: Application;

  /**
   * Creates an instance of Server.
   * @memberof Server
   */
  constructor() {
    super("Server", true);
    this.express = express();
    this.middleware()
    this.routes()
  }

  /**
   * Configures the express middleware
   *
   * @private
   * @memberof Server
   */
  private middleware() {
    const loggerConfig = {
      stream: {
        write: (meta) => {
          this.logger.info(meta);
        }
      }
    }
    this.express.use(cors())
    this.express.use(logger('tiny', loggerConfig));
    this.express.use(bodyParser.json())
    this.express.use(bodyParser.urlencoded({ extended: false }))
  }

  /**
   * Configures the server routes
   *
   * @private
   * @memberof Server
   */
  private routes() {
    // placeholder route handler
    this.express.use('/', express.static(join(__dirname, '..', '..', 'client', 'dist')));

    this.api()
  }

  /**
   * Applies the routers defined in ./routes to the route "/api"
   *
   * @private
   * @memberof Server
   */
  private api() {
    const router = Router();

    router.use("/message", MessageRouter);
    router.use("/module", ModuleRouter);
    router.use("/simulation", SimulationRouter);
    router.use("/result", ResultRouter);

    this.express.use("/api", router);
  }

  /**
   * Starts the server instance
   *
   * @param {number} [port=3000]
   * @param {string} [host="localhost"]
   * @returns
   * @memberof Server
   */
  public start(port = 3000, host = "localhost") {
    return new Promise((resolve) => {
      this.express.listen(port, host, () => {
        this.logger.info(`Server running on http://${host}:${port}`);
        resolve();
      });
    });
  }
}

export default Server;
